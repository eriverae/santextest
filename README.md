This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Project Title

Santex Test Battleship

### Synopsis

This project is a one-player version of the classic board game Battleship!

There will be 10 ships hidden in random locations on a square grid. The player will have some predefined moves to try to sink all of the ships.

## Installing

Installing
First: Clone project using this command through the terminal: git clone git clone https://eriverae@bitbucket.org/eriverae/santextest.git
Second: npm install in the project's root to install server dependencies
Third: Start App by using the command npm start
Running the tests
No automated tests in the proyect

## Author

Eddy Rivera
