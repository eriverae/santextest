import React, { useState } from "react";
import Grid from "./components/Grid";
import { Switch, Route } from "react-router-dom";
import styles from "./styles.module.scss";
import GameSetup from "./components/GameSetup";

function App() {
  const [turnsToPlay, setTurnsToPlay] = useState(1000);

  const handleChange = (e) => {
    let { value } = e.target;
    setTurnsToPlay(value);
  };
  return (
    <div className={styles.appContainer}>
      <Switch>
        <Route
          exact
          path="/"
          component={(props) => (
            <GameSetup
              onHandleChange={handleChange}
              option={turnsToPlay}
              {...props}
            />
          )}
        />
        <Route
          path="/board"
          component={(props) => (
            <Grid
              turns={turnsToPlay}
              setTurnsToPlay={setTurnsToPlay}
              {...props}
            />
          )}
        />
      </Switch>
    </div>
  );
}

export default App;
