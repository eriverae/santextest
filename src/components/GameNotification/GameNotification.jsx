import React from "react";
import { Link } from "react-router-dom";
import styles from "./styles.module.scss";

const GameNotification = ({
  shipBeaten,
  turnsLeft,
  gameCounter,
  firstRender,
}) => {
  let message = "";

  if (turnsLeft === 0) {
    message = "You Lose";
  }
  if (shipBeaten && turnsLeft !== 0) {
    message = "Ship Beaten";
  }
  if (!shipBeaten && turnsLeft !== 0) {
    message = "You missed";
  }
  if (gameCounter) {
    message = "You Won";
  }

  if (firstRender) {
    message = "";
  }

  const buttonPlayAgain =
    turnsLeft === 0 || gameCounter ? (
      <Link to="/" className={styles.link}>
        Try Again
      </Link>
    ) : null;

  return (
    <div className={styles.gameNotification}>
      {message}
      {buttonPlayAgain}
    </div>
  );
};

export default GameNotification;
