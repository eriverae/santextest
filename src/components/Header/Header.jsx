import React from "react";
import HeaderBlock from "../HeaderBlock";
import { columns } from "../../data";
import styles from "./styles.module.scss";

const Header = () => {
  const headerBlocks = columns.map((column) => {
    const { heading, index } = column;
    return <HeaderBlock heading={heading} key={index} />;
  });
  return <div className={styles.headerContainer}>{headerBlocks}</div>;
};

export default Header;
