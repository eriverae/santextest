import React from "react";
import RowBlock from "../RowBlock";
import styles from "./styles.module.scss";

const Row = ({
  columns,
  row,
  changeIsHitValue,
  changeLocalTurns,
  changeValueIsHit,
  turnsLeft,
  gameCounter,
  setFirstRender,
}) => {
  const blocks = columns.map((column, index) => {
    return (
      <RowBlock
        row={row}
        column={column}
        key={index}
        changeIsHitValue={changeIsHitValue}
        changeLocalTurns={changeLocalTurns}
        changeValueIsHit={changeValueIsHit}
        turnsLeft={turnsLeft}
        gameCounter={gameCounter}
        setFirstRender={setFirstRender}
      />
    );
  });
  return <div className={styles.cellContainer}>{blocks}</div>;
};

export default Row;
