import React from "react";
import styles from "./styles.module.scss";

const GameSetup = (props) => {
  const handleSubmit = () => {
    props.history.push("/board");
  };

  return (
    <div className={styles.gameSetupContainer}>
      <div className={styles.formContainer}>
        <form onSubmit={handleSubmit}>
          <label className={styles.label}>Select difficulty:</label>
          <select
            className={styles.select}
            onChange={props.onHandleChange}
            value={props.option}
          >
            <option value="1000">Easy</option>
            <option value="100">Medium</option>
            <option value="50">Hard</option>
          </select>
          <input className={styles.inputForm} type="submit" value="Submit" />
        </form>
      </div>
    </div>
  );
};

export default GameSetup;
