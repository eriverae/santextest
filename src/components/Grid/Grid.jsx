import React, { useState, useEffect } from "react";
import Row from "../Row";
import Header from "../Header";
import GameNotification from "../GameNotification";
import { columns } from "../../data";
import { generateShips } from "../../utils/utils";
import styles from "./styles.module.scss";

const Grid = ({ turns }) => {
  let [localTurns, setLocalTurns] = useState(turns);
  const [rowData, setRowData] = useState([]);
  let [cellsToSink, setCellsToSink] = useState(0);
  const [gameCounter, setGameCounter] = useState(false);
  const [shipBeaten, setShipBeaten] = useState(false);
  const [firstRender, setFirstRender] = useState(true);

  useEffect(() => {
    let result;
    result = generateShips();
    setRowData(result.rowDataCopy);
    setCellsToSink(result.totalSelectedItems);
  }, []);

  useEffect(() => {
    if (gameCounter) {
      console.log("You won");
    }
  }, [gameCounter]);

  useEffect(() => {
    if (localTurns === 0) {
      console.log("You lose");
    }
  }, [localTurns]);

  const changeIsHitValue = (selectedCell) => {
    setLocalTurns(--localTurns);
    const rowDataCopy = [...rowData];
    rowDataCopy.map((row) => {
      Object.values(row).find((column) => {
        if (column.cellPosition === selectedCell) {
          setCellsToSink(cellsToSink - 1);
          column.isHit = true;
        }
      });
    });

    setRowData(rowDataCopy);

    checkIfUserWon(cellsToSink);

    setShipBeaten(true);
  };

  const changeTurnValue = () => {
    setLocalTurns(--localTurns);
  };

  const checkIfUserWon = (cellsToSink) => {
    if (cellsToSink === 1) {
      setGameCounter(true);
    }
  };

  const changeValueIsHit = () => {
    setShipBeaten(false);
  };

  const changeFirstRenderToFalse = () => {
    setFirstRender(false);
  };

  const rows = rowData.map((row, index) => {
    return (
      <Row
        row={row}
        columns={columns}
        key={index}
        changeIsHitValue={changeIsHitValue}
        changeLocalTurns={changeTurnValue}
        changeValueIsHit={changeValueIsHit}
        turnsLeft={localTurns}
        gameCounter={gameCounter}
        setFirstRender={changeFirstRenderToFalse}
      />
    );
  });
  return (
    <div className={styles.gridContainer}>
      <div>
        <Header />
        {rows}
      </div>

      <GameNotification
        shipBeaten={shipBeaten}
        turnsLeft={localTurns}
        gameCounter={gameCounter}
        firstRender={firstRender}
      />
    </div>
  );
};

export default Grid;
