import React from "react";
import styles from "./styles.module.scss";

const HeaderBlock = ({ heading }) => {
  return <div className={styles.headerCell}>{heading}</div>;
};

export default HeaderBlock;
