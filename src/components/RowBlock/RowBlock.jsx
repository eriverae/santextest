import React from "react";
import classnames from "classnames";
import styles from "./styles.module.scss";

const RowBlock = ({
  row,
  column: { property },
  changeIsHitValue,
  changeLocalTurns,
  changeValueIsHit,
  turnsLeft,
  gameCounter,
  setFirstRender,
}) => {
  const { cellPosition, isTaken, isHit } = row[property];
  const isShipBeaten = () => {
    if (isTaken && isHit === false) {
      changeIsHitValue(cellPosition);
      setFirstRender();
    } else {
      changeLocalTurns();
      changeValueIsHit();
      setFirstRender();
    }
  };
  const stylesContainer = classnames(styles.rowBlock, {
    [styles.hit]: isHit,
  });

  const rowName = cellPosition.length === 1 ? cellPosition : null;
  return (
    <div
      className={stylesContainer}
      onClick={isHit || turnsLeft === 0 || gameCounter ? null : isShipBeaten}
    >
      {rowName}
    </div>
  );
};

export default RowBlock;
