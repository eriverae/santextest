const columns = [
  {
    heading: "",
    index: 0,
    property: "columnCero",
  },
  {
    heading: 1,
    index: 1,
    property: "columnOne",
  },
  {
    heading: 2,
    index: 2,
    property: "columnTwo",
  },
  {
    heading: 3,
    index: 3,
    property: "columnThree",
  },
  {
    heading: 4,
    index: 4,
    property: "columnFour",
  },
  {
    heading: 5,
    index: 5,
    property: "columnFive",
  },
  {
    heading: 6,
    index: 6,
    property: "columnSix",
  },
  {
    heading: 7,
    index: 7,
    property: "columnSeven",
  },
  {
    heading: 8,
    index: 8,
    property: "columnEight",
  },
  {
    heading: 9,
    index: 9,
    property: "columnNine",
  },
  {
    heading: 10,
    index: 10,
    property: "columnTen",
  },
];

export default columns;
