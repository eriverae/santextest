const rowData = [
  {
    columnCero: {
      cellPosition: "A",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "A1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "A2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "A3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "A4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "A5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "A6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "A7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "A8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "A9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "A10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "B",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "B1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "B2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "B3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "B4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "B5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "B6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "B7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "B8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "B9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "B10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "C",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "C1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "C2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "C3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "C4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "C5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "C6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "C7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "C8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "C9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "C10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "D",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "D1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "D2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "D3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "D4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "D5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "D6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "D7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "D8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "D9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "D10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "E",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "E1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "E2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "E3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "E4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "E5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "E6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "E7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "E8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "E9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "E10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "F",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "F1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "F2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "F3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "F4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "F5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "F6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "F7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "F8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "F9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "F10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "G",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "G1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "G2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "G3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "G4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "G5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "G6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "G7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "G8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "G9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "G10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "H",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "H1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "H2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "H3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "H4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "H5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "H6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "H7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "H8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "H9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "H10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "I",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "I1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "I2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "I3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "I4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "I5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "I6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "I7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "I8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "I9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "I10",
      isTaken: false,
      isHit: false,
    },
  },
  {
    columnCero: {
      cellPosition: "J",
      isTaken: false,
      isHit: false,
    },
    columnOne: {
      cellPosition: "J1",
      isTaken: false,
      isHit: false,
    },
    columnTwo: {
      cellPosition: "J2",
      isTaken: false,
      isHit: false,
    },
    columnThree: {
      cellPosition: "J3",
      isTaken: false,
      isHit: false,
    },
    columnFour: {
      cellPosition: "J4",
      isTaken: false,
      isHit: false,
    },
    columnFive: {
      cellPosition: "J5",
      isTaken: false,
      isHit: false,
    },
    columnSix: {
      cellPosition: "J6",
      isTaken: false,
      isHit: false,
    },
    columnSeven: {
      cellPosition: "J7",
      isTaken: false,
      isHit: false,
    },
    columnEight: {
      cellPosition: "J8",
      isTaken: false,
      isHit: false,
    },
    columnNine: {
      cellPosition: "J9",
      isTaken: false,
      isHit: false,
    },
    columnTen: {
      cellPosition: "J10",
      isTaken: false,
      isHit: false,
    },
  },
];

export default rowData;
