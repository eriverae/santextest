import columns from "./columns";
import rowData from "./rowData";
import { ships, shipQuantitys } from "./ships";

export { columns, rowData, ships, shipQuantitys };
