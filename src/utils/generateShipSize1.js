import getRandomCellPosition from "./generateRandomCellPosition";
import {
  getRowSiblings,
  getColumnSiblings,
  checkIfThereIsSpaceOrTaken,
} from "./utils";

const generateShipSize1 = (
  rowsParsedData,
  columnParseData,
  rowData,
  elementsToChange,
  shipQuantity = 4
) => {
  let itemsToChange = new Set();
  const shipSize = 1;

  for (let index = 0; index < shipQuantity; index++) {
    const position = generatePosition();
    if (position) {
      const { cellPosition } = getRandomCellPosition(rowData);

      const { valuesToTheRight, valuesToTheLeft } = getRowSiblings(
        rowsParsedData,
        cellPosition,
        shipSize
      );
      if (elementsToChange.size < 20) {
        itemsToChange = checkIfThereIsSpaceOrTaken(
          valuesToTheRight,
          valuesToTheLeft,
          shipSize,
          rowData
        );
      }

      if (itemsToChange.size === 0 && elementsToChange.size < 20) {
        generateShipSize1(
          rowsParsedData,
          columnParseData,
          rowData,
          elementsToChange,
          (shipQuantity = 3)
        );
      }

      itemsToChange.forEach((item) => {
        elementsToChange.add(item.cellPosition);
      });

      elementsToChange.forEach((value) => {
        const columnFromCell = value.slice(1, 3);
        const rowToChange = value.slice(0, 1);
        const cellDataRow = rowData[rows[rowToChange]][columns[columnFromCell]];
        if ((cellDataRow.cellPosition = value)) {
          cellDataRow.isTaken = true;
        }
      });
      if (itemsToChange.size > 0) itemsToChange.clear();
    } else {
      const { cellPosition } = getRandomCellPosition(rowData);

      const { valuesToTheBottom, valuesToTheTop } = getColumnSiblings(
        columnParseData,
        cellPosition,
        shipSize
      );

      if (elementsToChange.size < 20) {
        itemsToChange = checkIfThereIsSpaceOrTaken(
          valuesToTheBottom,
          valuesToTheTop,
          shipSize,
          rowData
        );
      }

      if (itemsToChange.size === 0 && elementsToChange.size < 20) {
        generateShipSize1(
          rowsParsedData,
          columnParseData,
          rowData,
          elementsToChange,
          (shipQuantity = 2)
        );
      }

      itemsToChange.forEach((item) => {
        elementsToChange.add(item.cellPosition);
      });

      elementsToChange.forEach((value) => {
        const columnFromCell = value.slice(1, 3);
        const rowToChange = value.slice(0, 1);
        const cellData = rowData[rows[rowToChange]][columns[columnFromCell]];
        if ((cellData.cellPosition = value)) {
          cellData.isTaken = true;
        }
      });
      if (itemsToChange.size > 0) itemsToChange.clear();
    }
  }
  return elementsToChange;
};

const generatePosition = () => {
  // 0 is a vertical Position
  // 1 is a horizontal Position
  return Math.floor(Math.random() * 2) + 0;
};

const rows = {
  A: 0,
  B: 1,
  C: 2,
  D: 3,
  E: 4,
  F: 5,
  G: 6,
  H: 7,
  I: 8,
  J: 9,
};

const columns = {
  1: "columnOne",
  2: "columnTwo",
  3: "columnThree",
  4: "columnFour",
  5: "columnFive",
  6: "columnSix",
  7: "columnSeven",
  8: "columnEight",
  9: "columnNine",
  10: "columnTen",
};

export default generateShipSize1;
