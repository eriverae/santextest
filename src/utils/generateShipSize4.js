import getRandomCellPosition from "./generateRandomCellPosition";
import { getRowSiblings, checkIfThereIsSpaceOrTaken } from "./utils";

const generateShipSize4 = (rowsParsedData, rowDataCopy, elementsToChange) => {
  let itemsToChange = new Set();
  const shipSize = 4;
  const { cellPosition } = getRandomCellPosition(rowDataCopy);

  const { valuesToTheRight, valuesToTheLeft } = getRowSiblings(
    rowsParsedData,
    cellPosition,
    shipSize
  );
  itemsToChange = checkIfThereIsSpaceOrTaken(
    valuesToTheRight,
    valuesToTheLeft,
    shipSize,
    rowDataCopy
  );

  itemsToChange.forEach((item) => {
    elementsToChange.add(item.cellPosition);
  });

  elementsToChange.forEach((value) => {
    const columnFromCell = value.slice(1, 3);
    const rowToChange = value.slice(0, 1);
    const cellData = rowDataCopy[rows[rowToChange]][columns[columnFromCell]];
    if ((cellData.cellPosition = value)) {
      cellData.isTaken = true;
    }
  });

  return rowDataCopy;
};

const rows = {
  A: 0,
  B: 1,
  C: 2,
  D: 3,
  E: 4,
  F: 5,
  G: 6,
  H: 7,
  I: 8,
  J: 9,
};

const columns = {
  1: "columnOne",
  2: "columnTwo",
  3: "columnThree",
  4: "columnFour",
  5: "columnFive",
  6: "columnSix",
  7: "columnSeven",
  8: "columnEight",
  9: "columnNine",
  10: "columnTen",
};

export default generateShipSize4;
