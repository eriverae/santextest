import cloneDeep from "lodash/cloneDeep";

export const getRandomCellPosition = (rowData) => {
  const arrayPositionRowData = Math.floor(Math.random() * 10) + 0;
  const rowDataCopy = cloneDeep(rowData);
  for (const property in rowDataCopy) {
    delete rowDataCopy[property].columnCero;
  }
  const singleRow = rowDataCopy[arrayPositionRowData];
  const cell = randomCell(singleRow);
  if (cell.isTaken) getRandomCellPosition(rowData);
  return cell;
};

const randomCell = (singleRow) => {
  var keys = Object.keys(singleRow);
  const cell = singleRow[keys[(keys.length * Math.random()) << 0]];
  return cell;
};

export default getRandomCellPosition;
