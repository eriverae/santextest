import { rowData, columns } from "../data";
import generateShipSize4 from "./generateShipSize4";
import generateShipSize3 from "./generateShipSize3";
import generateShipSize2 from "./generateShipsSize2";
import generateShipSize1 from "./generateShipSize1";
import cloneDeep from "lodash/cloneDeep";

export const generateShips = () => {
  const rowsParsedData = getRowsData();
  const columnParseData = getColumnData();
  const rowDataCopy = cloneDeep(rowData);
  const rowsParsedDataCopy = cloneDeep(rowsParsedData);
  const columnParseDataCopy = cloneDeep(columnParseData);
  let rowsAndColumnsResult;
  let elementsToChange = new Set();
  rowsAndColumnsResult = generateShipSize4(
    rowsParsedDataCopy,
    rowDataCopy,
    elementsToChange
  );
  rowsAndColumnsResult = generateShipSize3(
    rowsParsedDataCopy,
    columnParseDataCopy,
    rowsAndColumnsResult,
    elementsToChange
  );
  rowsAndColumnsResult = generateShipSize2(
    rowsParsedDataCopy,
    columnParseDataCopy,
    rowsAndColumnsResult,
    elementsToChange
  );

  rowsAndColumnsResult = generateShipSize1(
    rowsParsedDataCopy,
    columnParseDataCopy,
    rowsAndColumnsResult,
    elementsToChange
  );

  const totalSelectedItems = elementsToChange.size;
  return { rowDataCopy, totalSelectedItems };
};

const getColumnData = () => {
  let columnsObject = {};

  const properties = columns
    .map((column) => {
      return column.property;
    })
    .slice(1);

  properties.forEach((property) => {
    columnsObject[property] = {};
  });

  rowData.forEach((row) => {
    properties.forEach((property) => {
      const rowProperty = row[property].cellPosition;
      columnsObject[property][rowProperty] = row[property];
    });
  });
  return columnsObject;
};

const getRowsData = () => {
  //  Rows data

  let rowObject = {};
  let columnValueCero = columns[0].property;

  rowData.forEach((row) => {
    const key = row[columnValueCero].cellPosition;
    rowObject[key] = {};
  });

  const keys = Object.keys(rowObject);

  const rowDataCopy = cloneDeep(rowData);

  keys.forEach((key) => {
    rowDataCopy.forEach((row) => {
      if (row.columnCero.cellPosition === key) {
        rowObject[key] = row;
      }
    });
  });

  for (const property in rowObject) {
    delete rowObject[property].columnCero;
  }
  return rowObject;
};

export const getRowSiblings = (rows, cell, shipSize) => {
  const rowLetter = cell.slice(0, 1);
  const specificRow = rows[rowLetter];
  const values = Object.values(specificRow);
  const cellIndex = values.findIndex((value) => {
    return value.cellPosition === cell;
  });
  const valuesToTheRight = getValuesToTheRight(values, cellIndex, shipSize);
  const valuesToTheLeft = getValuesToTheLeft(values, cellIndex, shipSize);
  return { valuesToTheRight, valuesToTheLeft };
};

const getValuesToTheRight = (values, cellIndex, shipSize) => {
  const finalIndex = cellIndex + shipSize;
  const result = values.slice(cellIndex, finalIndex);
  return result;
};

const getValuesToTheLeft = (values, cellIndex, shipSize) => {
  let startingIndex = cellIndex - shipSize + 1;
  if (startingIndex < 0) startingIndex = 0;
  const result = values.slice(startingIndex, cellIndex + 1);
  return result;
};

export const getColumnSiblings = (columns, cell, shipSize) => {
  const values = Object.values(columns);
  const cellIndex = values.findIndex((value) => {
    return value[cell];
  });
  const specificColumn = values[cellIndex];
  const valuesToTheTop = getValuesColumnTop(specificColumn, shipSize, cell);
  const valuesToTheBottom = getValuesColumnBottom(
    specificColumn,
    shipSize,
    cell
  );

  return { valuesToTheTop, valuesToTheBottom };
};

const getValuesColumnTop = (specificColumn, shipSize, cell) => {
  const { values, cellIndex } = getValueAndIndexColumn(specificColumn, cell);
  const valuesToTheTop = getValuesToTheTop(values, cellIndex, shipSize);
  return valuesToTheTop;
};

const getValuesToTheTop = (values, cellIndex, shipSize) => {
  let startingIndex = cellIndex - shipSize + 1;
  if (startingIndex < 0) startingIndex = 0;
  const result = values.slice(startingIndex, cellIndex + 1);
  return result;
};

const getValuesColumnBottom = (specificColumn, shipSize, cell) => {
  const { values, cellIndex } = getValueAndIndexColumn(specificColumn, cell);
  const valuesToTheBottom = getValuesToTheBottom(values, cellIndex, shipSize);
  return valuesToTheBottom;
};

const getValuesToTheBottom = (values, cellIndex, shipSize) => {
  const finalIndex = cellIndex + shipSize;
  const result = values.slice(cellIndex, finalIndex);
  return result;
};

const getValueAndIndexColumn = (specificColumn, cell) => {
  const values = Object.values(specificColumn);
  const cellIndex = values.findIndex((value) => {
    return value.cellPosition === cell;
  });
  return { values, cellIndex };
};

export const checkIfThereIsSpaceOrTaken = (
  firstValue,
  secondValue,
  shipSize,
  rowData
) => {
  const result = rowData.some((value) => {
    return firstValue.some((v) => {
      const cell = v.cellPosition.slice(1, 3);
      if (
        v.cellPosition === value[columnsData[cell]].cellPosition &&
        value[columnsData[cell]].isTaken
      ) {
        return value[columnsData[cell]].isTaken;
      }
    });
  });
  const firstValueSet = new Set([...firstValue]);
  const secondValueSet = new Set([...secondValue]);
  if (result) {
    return new Set([]);
  }

  if (firstValueSet.size === shipSize) {
    return firstValueSet;
  }

  return secondValueSet;
};

const columnsData = {
  1: "columnOne",
  2: "columnTwo",
  3: "columnThree",
  4: "columnFour",
  5: "columnFive",
  6: "columnSix",
  7: "columnSeven",
  8: "columnEight",
  9: "columnNine",
  10: "columnTen",
};
